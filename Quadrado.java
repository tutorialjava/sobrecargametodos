package padroesprojeto;

public class Quadrado implements IFiguraBiDimensional{

	private int lado;
	
	public int getLado() { /* Retorna o valor do lado*/
		return this.lado;
	}
	
	public void setLado(int valor) { /* Set o valor do lado*/
		this.lado = valor;
	}
	
	public Quadrado() {
		this.lado = 5;
	}
	
	@Override
	public double perimetro() {
		return lado * 4;
	}
	

	@Override
	public double area() {
		return lado * lado;
	}
	
	@Override
	public String toString() {
		return "(" + this.lado + ", " + this.lado + ", " + this.lado + ", " + this.lado + ")";
	}

}
