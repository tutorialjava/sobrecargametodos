package padroesprojeto;

public class Circulo implements IFiguraBiDimensional {

	private int raio;
	
	public Circulo(int raio) { /* Inicializa um circulo com um raio*/
		this.raio = raio;
	}
	public Circulo() { 
		this.raio = 5;
	}
	
	public int getRaio() {
		return this.raio;
	}
	public void setRaio(int valor) { /* Seta o valor do Circulo*/
		this.raio = valor;
	}
	
	@Override
	public double perimetro() { 
		return 2 * Math.PI * raio;
	}

	@Override
	public double area() { /* Area do circulo é Pi vezes area ao quadrado*/
		return Math.PI * Math.pow(raio, 2);
		
	}

}
