package padroesprojeto;

public class Triangulo implements IFiguraBiDimensional{

	private int ladoA;
	private int ladoB;
	private int ladoC;

	private boolean condicaoExistencia(int a, int b, int c) {
		return Math.abs(b - c) < a && a < b + c;
	}

	public Triangulo(int a) {
		if(a > 0) {
			this.ladoA = a;
			this.ladoB = a;
			this.ladoC = a;			
		}else {
			this.ladoA = 1;
			this.ladoB = 1;
			this.ladoC = 1;
		}
	}
	
	public Triangulo(int a, int b, int c) {

		if (!condicaoExistencia(a, b, c)) {
			throw new RuntimeException("Impossi­vel construir triangulo");
		}

		this.ladoA = a;
		this.ladoB = b;
		this.ladoC = c;
	}

	@Override
	public String toString() {
		return "(" + this.ladoA + ", " + this.ladoB + ", " + this.ladoC + ")";
	}

	public int getLadoA() { /*Retorna o valor do lado A*/
		return ladoA;
	}
	public int getLadoB() { /*Retorna o valor do lado B*/
		return ladoB;
	}
	public int getLadoC() { /*Retorna o valor do lado C*/
		return ladoC;
	}

	public void setLadoA(int valor) { /*Seta se condizer com as condições de existencia o valor do lado A*/
		if (this.condicaoExistencia(valor, this.ladoB, this.ladoC)) {
			this.ladoA = valor;
		}
	}
	public void setLadoB(int valor) { /*Seta se condizer com as condições de existencia o valor do lado B*/
		if (this.condicaoExistencia(this.ladoA, valor, this.ladoC)) {
			this.ladoB = valor;
		}
	}
	public void setLadoC(int valor) { /*Seta se condizer com as condições de existencia o valor do lado C*/
		if (this.condicaoExistencia(this.ladoA, this.ladoB, valor)) {
			this.ladoC = valor;
		}		
	}

	public double perimetro() {
		return ladoA + ladoB + ladoC;
	}
	
	public double area() {
		
		double s = perimetro() / 2;
		
		double sa = s * (s - ladoA) * (s - ladoB) * (s - ladoC);
				
		return Math.sqrt(sa);
	}
}
