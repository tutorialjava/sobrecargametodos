package padroesprojeto;

public class Retangulo implements IFiguraBiDimensional {

	private int comprimento;
	private int largura;
	
	public int getComprimento() { 
		return comprimento;
	}
	public void setLargura(int valor) { /*Gera um novo valor, se bater com as condições de existencia ele da um set no valor */
		if(this.condicaoExistencia(this.comprimento, valor)) {
			this.largura = valor;
		}
	}

	public void setComprimento(int valor) {
		if(this.condicaoExistencia(valor, this.largura)) {
			this.comprimento = valor;
		}
	}

	public int getLargura() { 
		return largura;
	}

	
	
	private boolean condicaoExistencia(int c, int l) {
		return c > 0 && l > 0;
	}
	
	public Retangulo() {
		this.comprimento = 1;
		this.largura = 2;
	}

	public Retangulo(int comprimento, int largura) {
		
		if (!condicaoExistencia(comprimento, largura)) {
			throw new RuntimeException("ImpossÃ­vel construir retÃ¢ngulo.");
		}
		
		this.comprimento = comprimento;
		this.largura = largura;
	}

	

	public double perimetro() {
		return comprimento + comprimento + largura + largura;
	}

	public double area() {
		return comprimento * largura;
	}

	@Override
	public String toString() {
		return "(" + this.comprimento + ", " + this.comprimento + ", " + this.largura + ", " + this.largura + ")";
	}

}
